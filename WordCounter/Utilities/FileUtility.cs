using System.IO;
using System.Text;
using WordCounter.Objects;

namespace WordCounter.Utilities
{
    internal class FileUtility 
    {
        private readonly string _path;

        public FileUtility(string path)
        {
            _path = path;
        }

        private const int BufferSize = 4096;

        // Creates a list of all words, defined as all consecutive sequences of characters [a-zA-Z]
        public List<byte[]> ReadWords()
        {
            var stack = new List<byte[]>();

            var readBuffer = new byte[BufferSize];

            using (Stream
                fileStream = new FileStream(_path, FileMode.Open, FileAccess.Read, FileShare.Read, BufferSize),
                memoryStream = new MemoryStream())
            {
                int bytesRead;

                do
                {
                    // Read the next chunk to the buffer
                    bytesRead = fileStream.Read(readBuffer, 0, BufferSize);

                    for (var i = 0; i < bytesRead; i++)
                    {
                        // If the current byte is a valid character, then write it to the memorystream
                        if (char.IsLetter((char)readBuffer[i]))
                        {
                            memoryStream.WriteByte(readBuffer[i]);
                            continue;
                        }

                        // If the current character is not valid, then the end of a word has been reached
                        // The memorystream will now either be empty or contain a word to be processed

                        var length = memoryStream.Position;

                        // If empty, move on...
                        if (length == 0)
                        {
                            continue;
                        }

                        // Read bytes from the memorystream
                        var word = new byte[length];

                        memoryStream.Seek(0L, SeekOrigin.Begin);
                        memoryStream.Read(word, 0, (int) length);

                        // Add the entry to the value store
                        stack.Add(word);

                        // Reset pointer
                        memoryStream.Seek(0L, SeekOrigin.Begin);
                    }

                // If the number of bytes read does not match the specified buffer size, then exit
                } while (bytesRead == BufferSize);
            }

            return stack;
        }


        // Shameless copy/paste!
        public Encoding GetEncoding()
        {
            // Read the BOM
            var bom = new byte[4];
            using (var file = new FileStream(_path, FileMode.Open, FileAccess.Read))
            {
                file.Read(bom, 0, 4);
            }

            // Analyze the BOM
            if (bom[0] == 0x2b && bom[1] == 0x2f && bom[2] == 0x76) return Encoding.UTF7;
            if (bom[0] == 0xef && bom[1] == 0xbb && bom[2] == 0xbf) return Encoding.UTF8;
            if (bom[0] == 0xff && bom[1] == 0xfe) return Encoding.Unicode; //UTF-16LE
            if (bom[0] == 0xfe && bom[1] == 0xff) return Encoding.BigEndianUnicode; //UTF-16BE
            if (bom[0] == 0 && bom[1] == 0 && bom[2] == 0xfe && bom[3] == 0xff) return Encoding.UTF32;

            return Encoding.Default;
        }
    }
}