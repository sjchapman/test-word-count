using System;

namespace WordCounter.Utilities
{
    public class MergeSort<T>
    {
        private readonly Func<T,T,bool> _comparison;

        /// <param name="comparison">
        /// A function to compare the left and right values. Should return true 
        /// if they are in the correct order, or false if they are not.
        /// </param>
        public MergeSort(Func<T,T,bool> comparison)
        {
            _comparison = comparison;
        }

        public void Sort(T[] toSort)
        {
            T[] workArray = new T[toSort.Length];

            SplitAndMerge(toSort, 0, toSort.Length, workArray);
        }

        private void SplitAndMerge(T[] toSort, int begin, int end, T[] workArray)
        {
            // If there's only one item then it can't be split; return.
            if(end - begin < 2)
            {
                return;
            }

            var middle = (end + begin) / 2;

            // Recursively split into halves
            SplitAndMerge(toSort, begin, middle, workArray);
            SplitAndMerge(toSort, middle, end, workArray);

            // Merge the two halves again
            Merge(toSort, begin, middle, end, workArray);

            // Copy the workArray back to the sort array
            for (var i = begin; i < end; i++)
            {
                toSort[i] = workArray[i];
            }
        }

        // Merge toSort from (begin)->(middle-1) and (middle)->(end-1)
        private void Merge(T[] toSort, int begin, int middle, int end, T[] workArray)
        {
            int left = begin, 
                right = middle;
            
            // Fill the work array by comparing the leftmost values of left and right
            for (var i = begin; i < end; i++) {
                // If left run head exists and is <= existing right run head.

                // If the left value is less than the right value
                if (left < middle && (right >= end || _comparison(toSort[left], toSort[right])))
                {
                    workArray[i] = toSort[left];
                    left = left + 1;
                }
                else
                {
                    workArray[i] = toSort[right];
                    right = right + 1;    
                }
            } 
        }
        
    }
}
