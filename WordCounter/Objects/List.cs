using System;
using WordCounter.Utilities;

namespace WordCounter.Objects
{
    internal class List<T>
    {
        private T[] _values;
        private int _size;

        public List()
        {
            _values = new T[] {};
            _size = 0;
        }

        public int Size
        {
            get { return _size; }
        }

        public T Last {
            // Return the last value in the list
            get { return _values[_size - 1]; }
        }

        public void Add(T item)
        {
            // If the array is full then double it in size
            if (_size >= _values.Length)
            {
                Array.Resize(ref _values, (_size+1) * 2);
            }

            // And add it to the end
            _values[_size++] = item;
        }

        public bool Take(out T value)
        {
            // If the array is empty, then return false
            if (_size <= 0)
            {
                value = default(T);
                return false;
            }

            // Otherwise get the last value...
            value = Last;

            // Delete it...
            _values[--_size] = default(T);

            // Resize the array (if necessary)...
            if (_values.Length > _size * 2)
            {
                Array.Resize(ref _values, _size);
            }

            // And return true
            return true;
        }

        public void Sort(Func<T,T,bool> leftRightComparison)
        {
            var sorter = new MergeSort<T>(leftRightComparison);

            if (_values.Length != _size)
            {
                Array.Resize(ref _values, _size);
            }

            sorter.Sort(_values);
        }

        // Copy the data to a new array and return it
        public T[] CopyData()
        {
            var newArray = new T[_size];

            for (var i = 0; i < _size; i++)
            {
                newArray[i] = _values[i];
            }

            return newArray;
        }

        public void Apply(Action<T> action)
        {
            Apply(action, 0, _size);
        }

        public void Apply(Action<T> action, int start, int count)
        {
            var limit = Math.Min(count, _size);

            for (var i = start; i < limit; i++)
            {
                action(_values[i]);
            }
        }
    }
}