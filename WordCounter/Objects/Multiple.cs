namespace WordCounter.Objects
{
    public class Multiple<T>
    {
        public T Value { get; private set; }
        public int Count { get; private set; }

        public Multiple(T value, int count)
        {
            Value = value;
            Count = count;
        }

        public void Increment()
        {
            Count++;
        }
    }
}