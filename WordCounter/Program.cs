﻿using System;
using System.Text;
using WordCounter.Objects;
using WordCounter.Utilities;

namespace WordCounter
{
    class Program
    {
        static void Main(string[] args)
        {
            var start = DateTime.Now;

            OutputTopTwentyWordsInFile(args[0]);

            var end = DateTime.Now;

            Console.WriteLine("\nTook {0}ms", end.Subtract(start).TotalMilliseconds);
            Console.ReadLine();
        }

        /*
         * Broadly, this goes through a few steps to get the final result.
         * 
         * 1. Extract list of all words in the file
         * 2. Build list of word counts
         * 3. Order word counts by count
         * 4. Print results
         */
        private static void OutputTopTwentyWordsInFile(string path)
        {
            // ----------------------------------------
            // 1. Extract list of all words in the file


            var file = new FileUtility(path);

            // Get the appropariate encoding to use for the file
            var encoding = file.GetEncoding();

            // Read in a list of every word from the given file path
            List<byte[]> words = file.ReadWords();


            // ----------------------------------------
            // 2. Build list of word counts


            // Convert them all to lowercase before sorting
            words.Apply(word => ToLower(word, encoding));

            // Sort using comparison function
            words.Sort((left, right) =>
            {    
                // Loop through the smallest array
                var min = Math.Min(left.Length, right.Length);

                for (var i = 0; i <= min; i++)
                {
                    // If an array has run out of values
                    if (i == min)
                    {
                        // Left is in the correct place if it is shorter than or equal to 
                        // right in length; otherwise it should be swapped
                        return left.Length == min;
                    }

                    // Compare values and return as appropriate if mismatched
                    if (left[i] < right[i]) return true;
                    if (left[i] > right[i]) return false;
                }

                // In the case both words match, left is in the correct place.
                return true;
            
            });

            var wordCount = new List<Multiple<byte[]>>();

            byte[] previous = {};
            byte[] current;

            while (words.Take(out current))
            {
                // Check if current value matches previous
                if (Equals(current, previous))
                {
                    // If so, increment most recent value in list
                    wordCount.Last.Increment();
                }
                else
                {
                    // If not, then add a new value to the word count list
                    wordCount.Add(new Multiple<byte[]>(current, 1));
                }

                // Assign current to previous and move on...
                previous = current;
            }


            // ----------------------------------------
            // 3. Order word counts by count


            // Sort the wordCount list by the count DESC
            wordCount.Sort((left, right) => left.Count > right.Count);


            // ----------------------------------------
            // 4. Print results

            // For each word, apply the following delegate
            wordCount.Apply(word => Console.WriteLine("{0} {1}", encoding.GetString(word.Value), word.Count), 0, 20);
        }

        private static void ToLower(byte[] buffer, Encoding encoding)
        {
            // Convert to chars with correct encoding
            var chars = encoding.GetChars(buffer);

            // ensure all characters are lowercase
            for (var i = 0; i < chars.Length; i++)
            {
                buffer[i] = (byte)char.ToLower(chars[i]);
            }
        }

        private static bool Equals(byte[] a, byte[] b)
        {
            // if their lengths don't match they obviously aren't equal!
            if (a.Length != b.Length)
            {
                return false;
            }

            var i = 0;

            // count the number of matching bytes
            while (++i < a.Length && a[i] == b[i]) { }

            // If the number of matching bytes is the same as their length, they match
            return i >= a.Length;
        }
    }
}
